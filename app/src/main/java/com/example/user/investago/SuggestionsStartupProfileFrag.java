package com.example.user.investago;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class SuggestionsStartupProfileFrag extends Fragment {

    ArrayList<SuggestionsDetails> suggest;
    FirebaseAuth mauth;
boolean check=false;
    DatabaseReference mdatabase;

    public SuggestionsStartupProfileFrag() {
        // Required empty public constructor
    }

    View v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ListView ll = (ListView) view.findViewById(R.id.sugList);
        suggest = new ArrayList<>();
        mauth=FirebaseAuth.getInstance();
        mdatabase=FirebaseDatabase.getInstance().getReference().child("startup").child(mauth.getCurrentUser().getUid()).child("comment");


        final SuggestionsAdapter sg = new SuggestionsAdapter(getActivity(),R.layout.suggestions_item,suggest);
        ll.setAdapter(sg);

        final ProgressDialog progressDialog=new ProgressDialog(getContext());
        progressDialog.setTitle("Loading...");
        progressDialog.show();


        mdatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                SuggestionsDetails ss = dataSnapshot.getValue(SuggestionsDetails.class);

                suggest.add(ss);
                if(check==false){
                    progressDialog.dismiss();
                    check=true;
                }
                sg.notifyDataSetChanged();



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

       /* ///delete this///
        SuggestionsDetails ss = new SuggestionsDetails();
        ss.setComment("cool idea!!");
        ss.setUser("Nishijeet");
        ss.setCompany("DEZVOLTA");
        suggest.add(ss);
        /////////////////////*/


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.suggestions_startupprofile_frag, container, false);
    }
}