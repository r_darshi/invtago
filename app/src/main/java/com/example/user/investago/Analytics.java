package com.example.user.investago;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class Analytics extends AppCompatActivity {

    LinearLayout lyy;
    private Animation fab_open, fab_close;
    private Boolean isFabOpen = false;
    GraphView graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.analytics);

        lyy = (LinearLayout) findViewById(R.id.layy);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.close);

        graph = (GraphView) findViewById(R.id.graph);

        FloatingActionButton drop = (FloatingActionButton) findViewById(R.id.drop);
        drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });
    }


    public void tech_comp(View v) {

        // inflate("5 most valuable Tech Companies");

        graph.removeAllSeries();

        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[]{
                new DataPoint(0, 611),
                new DataPoint(1, 400),
                new DataPoint(2, 384),
                new DataPoint(3, 227),
                new DataPoint(4, 200)
        });
        graph.addSeries(series);

// styling
        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX() * 255 / 4, (int) Math.abs(data.getY() * 255 / 6), 100);
            }
        });

        series.setSpacing(50);

// draw values on top
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.RED);

        TextView txt = (TextView) findViewById(R.id.txt);
        txt.setText("Y- Billion Dollars;\n X- (0:Apple),(1:Google),(2:Microsoft),(3:Alibaba),(4:Facebook);");

    }

    public void tech_vs_nontech(View v) {


        graph.removeAllSeries();

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(2012, 8),
                new DataPoint(2013, 11),
                new DataPoint(2014, 10),
                new DataPoint(2015, 16),
                new DataPoint(2016, 14)
        });
        series.setColor(Color.rgb(0, 0, 255));
        graph.addSeries(series);

        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(2012, 13),
                new DataPoint(2013, 11),
                new DataPoint(2014, 11),
                new DataPoint(2015, 9),
                new DataPoint(2016, 8)
        });
        series2.setColor(Color.rgb(0, 255, 0));
        graph.addSeries(series2);

        TextView txt = (TextView) findViewById(R.id.txt);
        txt.setText("Y- Return On Investment(%);\n X- Year;\nBlue: Tech & Green: Non Tech");

    }


    public void upstream(View v) {


        graph.removeAllSeries();

        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[]{
                new DataPoint(2012, 60),
                new DataPoint(2013, 20),
                new DataPoint(2014, 55),
                new DataPoint(2015, 223),
                new DataPoint(2016, 289)
        });
        series.setColor(Color.rgb(0, 0, 255));
        series.setDataWidth(1);
        graph.addSeries(series);

        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(2012, 55),
                new DataPoint(2013, 25),
                new DataPoint(2014, 50),
                new DataPoint(2015, 213),
                new DataPoint(2016, 274)
        });
        series2.setColor(Color.rgb(0, 255, 0));
        series2.setThickness(3);
        graph.addSeries(series2);

        TextView txt = (TextView) findViewById(R.id.txt);
        txt.setText("Y- Millions of Equivalence;\n X- Year;\nBlue: Production & Green: Proved Reserves");

    }


    public void animateFAB() {  // animate the expandable floatingAction Button

        if (isFabOpen) {
            lyy.startAnimation(fab_close);
            lyy.setClickable(false);
            isFabOpen = false;
            Log.d("nj", "close");

        } else {
            lyy.startAnimation(fab_open);
            lyy.setClickable(true);
            isFabOpen = true;
            Log.d("nj", "open");

        }
    }

}