package com.example.user.investago;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jackandphantom.circularprogressbar.CircleProgressbar;

import java.util.ArrayList;


public class StartupAdapter extends ArrayAdapter<StartUpDetails> { // A simple custom listview adapter.


    ArrayList<StartUpDetails> list = new ArrayList<>();
    Context con;


    public StartupAdapter(Context context, int resource, ArrayList<StartUpDetails> list) { // list is passed through constructor
        super(context, resource);
        this.con = context;
        this.list = list;
    }

    public int getCount() {
        return this.list.size();
    }

    public StartUpDetails getItem(int index) {
        return this.list.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.startup_item, parent, false);
        }

        TextView title = (TextView) row.findViewById(R.id.title);
        TextView company = (TextView) row.findViewById(R.id.company);
        TextView name = (TextView) row.findViewById(R.id.Name);
        TextView worth1 = (TextView) row.findViewById(R.id.worth1);
        ImageView back = (ImageView) row.findViewById(R.id.back_img);

        /*StartUpDetails pr = getItem(position);*/
        StartUpDetails pr = list.get(position);

        title.setText(pr.getTitle());
        company.setText(pr.getCompany());
        worth1.setText("Worth: " + pr.getWorth());
        Glide.with(con).load(pr.getUri()).into(back);
        back.setScaleType(ImageView.ScaleType.CENTER_CROP);
        name.setText(pr.getName());

        CircleProgressbar circleProgressbar = (CircleProgressbar) row.findViewById(R.id.worth);
        circleProgressbar.setForegroundProgressColor(Color.GREEN);
        circleProgressbar.setBackgroundColor(Color.TRANSPARENT);
        circleProgressbar.setBackgroundProgressWidth(10);
        circleProgressbar.setForegroundProgressWidth(9);
        circleProgressbar.enabledTouch(false);
        circleProgressbar.setRoundedCorner(true);
        circleProgressbar.setClockwise(false);
        int animationDuration = 8000; // 2500ms = 2,5s
        if (isNumeric(pr.getWorth()))
            circleProgressbar.setProgressWithAnimation((Integer.parseInt(pr.getWorth()) / 1000), animationDuration); // Default duration = 1500ms
        return row;
    }

    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
}