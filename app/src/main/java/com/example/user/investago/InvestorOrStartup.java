package com.example.user.investago;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

public class InvestorOrStartup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.startup_investor);
    }

    public void startup(View v){

      startActivity(new Intent(InvestorOrStartup.this, LoginStartup.class));

    }

    public void investor(View v){

        startActivity(new Intent(InvestorOrStartup.this,LoginInvestor.class));

    }
}