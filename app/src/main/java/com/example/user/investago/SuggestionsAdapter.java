package com.example.user.investago;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jackandphantom.circularprogressbar.CircleProgressbar;

import java.util.ArrayList;


public class SuggestionsAdapter extends ArrayAdapter<SuggestionsDetails> { // A simple custom listview adapter.


    ArrayList<SuggestionsDetails> list = new ArrayList<>();
    Context con;


    public SuggestionsAdapter(Context context, int resource, ArrayList<SuggestionsDetails> list) { // list is passed through constructor
        super(context, resource);
        this.con = context;
        this.list = list;
    }

    public int getCount() {
        return this.list.size();
    }

    public SuggestionsDetails getItem(int index) {
        return this.list.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.suggestions_item, parent, false);
        }

        TextView name = (TextView) row.findViewById(R.id.name);
        TextView company = (TextView) row.findViewById(R.id.company);
        TextView comment = (TextView) row.findViewById(R.id.comment);

        /*SuggestionsDetails pr = getItem(position);*/
        SuggestionsDetails pr = list.get(position);

        name.setText(pr.getInvestor_name());
        company.setText(pr.getInvestors_comapny_name());
        comment.setText(pr.getComment());

        return row;
    }
}