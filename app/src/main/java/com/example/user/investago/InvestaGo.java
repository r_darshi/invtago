package com.example.user.investago;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

public class InvestaGo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.investago);

        if (isNetworkAvailable()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    }

                    Intent i = new Intent(InvestaGo.this, InvestorOrStartup.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  // clearing the stack so that you
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);// dont get back to the login screen
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);// on pressing back button


                    startActivity(i);

                }
            }).start();

        } else {
            Toast.makeText(InvestaGo.this, "Internet Connection Requiered!", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}