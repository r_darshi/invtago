package com.example.user.investago;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.Image;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class StartupProfile extends AppCompatActivity {
    ImageView profile;
    private TextView name, company, title, worth, email;
    private DatabaseReference mdatabase= FirebaseDatabase.getInstance().getReference().child("startup");
    private String Email;
    private FirebaseAuth mauth;
    private SharedPreferences sharedPreferences_in;
    private String Value;
    private EditText editText;

    //SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_startup_profile);
        profile = (ImageView) findViewById(R.id.profile_image);
        name = (TextView) findViewById(R.id.name);
        company = (TextView) findViewById(R.id.comp);
        title = (TextView) findViewById(R.id.tit);
        worth = (TextView) findViewById(R.id.worth1);
        email = (TextView) findViewById(R.id.email);
        mauth=FirebaseAuth.getInstance();

        Glide.with(StartupProfile.this).load(null).into(profile);
        name.setText("");
        company.setText("");
        title.setText("");
        worth.setText("");
        email.setText("");

//        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String s = getIntent().getStringExtra("Details");
        String to[] = s.split(";;");
        Log.i("CkeckS", s);
        Email = to[1];
        Log.i(":::Email",Email);
        Log.i(":::Email",Email);
        Log.i(":::Email",Email);
        Log.i(":::Email",Email);
        Log.i(":::Email",Email);

        String Company = to[0];
        String Title = to[3];
        String Worth = to[5];
        String Name = to[2];
        String Uri = to[4];

        Log.i("CkeckSiowa", Name + " " + Company + " " + Title + " " + Worth + " " + Email + " " + Uri);

        // Toast.makeText(this,s,Toast.LENGTH_LONG).show();

        if (!Uri.isEmpty() || Uri.length() != 0)
            Glide.with(StartupProfile.this).load(Uri).into(profile);
        name.setText(Name);
        company.setText(Company);
        title.setText(Title);
        worth.setText(Worth);
        email.setText(Email);

    }

    public void review(View v) {

        final AlertDialog.Builder customDialog
                = new AlertDialog.Builder(StartupProfile.this);
        customDialog.setTitle("Suggestions");

        LayoutInflater layoutInflater
                = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.comment, null);
        customDialog.setView(view);
        customDialog.show();
        editText=(EditText)view.findViewById(R.id.dial_ed);

        sharedPreferences_in= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String Name=sharedPreferences_in.getString("investor name","--");
        final String Company=sharedPreferences_in.getString("inv company","--");



        Button post = (Button) view.findViewById(R.id.post);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Value=editText.getText().toString().trim();
                Log.i("Value",Value);
                if(!Value.equals("")){
                    Log.i("comment",Value);
                    Query query=mdatabase.orderByChild("email").equalTo(Email);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            // There may be multiple users with the email address, so we need to loop over the matches
                            for (DataSnapshot userSnapshot: dataSnapshot.getChildren()) {
                                Log.i("Keykey",String.valueOf(userSnapshot.getKey()));
                                Log.i("Keykey",String.valueOf(userSnapshot.getKey()));
                                Log.i("Keykey",String.valueOf(userSnapshot.getKey()));
                                Log.i("Keykey",String.valueOf(userSnapshot.getKey()));
                                Log.i("Keykey",String.valueOf(userSnapshot.getKey()));


                                String key=String.valueOf(userSnapshot.getKey());
                                HashMap<String,String> val=new HashMap<String, String>();
                                val.put("investor name",Name);
                                val.put("comment",Value);
                                val.put("investors comapny name",Company);
                                mdatabase.child(key).child("comment").push().setValue(val).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        Toast.makeText(StartupProfile.this, "Message Posted", Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(StartupProfile.this, "Failed to Post the message", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Getting Post failed, log a message
                            Log.i("onCancelled", String.valueOf(databaseError.toException()));
                            // ...
                        }
                    });

                    customDialog.setCancelable(true);
                }
            }
        });


    }
}
