package com.example.user.investago;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class InvestorList_Frag1 extends Fragment { ////////////populate investor list here

    boolean check=false;

    ArrayList<InvestorDetails> inv;
    DatabaseReference mdatabase;
    InvestorAdapter ia;
    public InvestorList_Frag1() {
        // Required empty public constructor
    }

    View v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ListView ll = (ListView) view.findViewById(R.id.inList);
        inv = new ArrayList<>();
        ia = new InvestorAdapter(getContext(), R.layout.investor_item, inv);
        ll.setAdapter(ia);
        final ProgressDialog progressDialog=new ProgressDialog(getContext());
        progressDialog.setTitle("Loading...");
        progressDialog.show();



        mdatabase= FirebaseDatabase.getInstance().getReference().child("investor");
        mdatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                InvestorDetails det = dataSnapshot.getValue(InvestorDetails.class);
                Log.i("Uri",det.getUri());
                Log.i("Uri",det.getUri());
                Log.i("Email",det.getEMail());
                Log.i("Uri",det.getUri());


                inv.add(det);
                if(check==false){
                    progressDialog.dismiss();
                    check=true;
                }
                ia.notifyDataSetChanged();



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                InvestorDetails st = inv.get(position);
                String passable = st.getCompany() + ";;" + st.getEMail() + ";;" + st.getName() + ";;" + st.getUri();
                Log.i("Passableiowa", passable);
                Intent i = new Intent(getActivity(), InvestorProfileForList.class);
                i.putExtra("Details", passable);
                startActivity(i);

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.investor_list_frag, container, false);
    }

}