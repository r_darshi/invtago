package com.example.user.investago;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


public class StartupList_Frag2 extends Fragment{
    boolean check=false;
    public StartupList_Frag2() {
        // Required empty public constructor
    }

    ArrayList<StartUpDetails> inv;
    DatabaseReference db;
    StartupAdapter ia;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ListView ll = (ListView) view.findViewById(R.id.stList);
        inv = new ArrayList<>();
        db=FirebaseDatabase.getInstance().getReference().child("startup");

        ia = new StartupAdapter(getContext(), R.layout.startup_item, inv);
        ll.setAdapter(ia);
        final ProgressDialog progressDialog=new ProgressDialog(getContext());
        progressDialog.setTitle("Loading...");
        progressDialog.show();
        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                StartUpDetails details = dataSnapshot.getValue(StartUpDetails.class);
                inv.add(details);
                if(check==false){
                    progressDialog.dismiss();
                    check=true;
                }
                ia.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        ll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StartUpDetails st = inv.get(position);
                String passable = st.getCompany() + ";;" + st.getEMail() + ";;" + st.getName() + ";;" + st.getTitle() + ";;" + st.getUri() + ";;" + st.getWorth();
                Log.i("Passableiowa",passable);
                Intent i = new Intent(getActivity(), StartupProfile.class);
                i.putExtra("Details", passable);
                startActivity(i);

            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.startup_list_frag2, container, false);
    }

}