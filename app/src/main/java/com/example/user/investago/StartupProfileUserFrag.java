package com.example.user.investago;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class StartupProfileUserFrag extends Fragment {
    SharedPreferences sharedPreferences_in;


    ImageView profile;
    TextView name, company, title, worth, email;
    FloatingActionButton suggestt;


    public StartupProfileUserFrag() {
        // Required empty public constructor
    }

    View v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        sharedPreferences_in = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        String Company = sharedPreferences_in.getString("cmpany", "--");
        String Title = sharedPreferences_in.getString("title", "--");
        String Email = sharedPreferences_in.getString("email", "--");
        String Uri = sharedPreferences_in.getString("uri", "--");
        String Name = sharedPreferences_in.getString("name", "--");
        String Worth = sharedPreferences_in.getString("worth", "--");

        profile = (ImageView) view.findViewById(R.id.profile_image);
        name = (TextView) view.findViewById(R.id.name);
        company = (TextView) view.findViewById(R.id.comp);
        title = (TextView) view.findViewById(R.id.tit);
        worth = (TextView) view.findViewById(R.id.worth1);
        email = (TextView) view.findViewById(R.id.email);
        suggestt = (FloatingActionButton) view.findViewById(R.id.suggestt);
        suggestt.setVisibility(View.INVISIBLE);

        Glide.with(getActivity()).load(null).into(profile);
        name.setText("");
        company.setText("");
        title.setText("");
        worth.setText("");
        email.setText("");

        if (!Uri.isEmpty() || Uri.length() != 0)
            Glide.with(getActivity()).load(Uri).into(profile);
        name.setText(Name);
        company.setText(Company);
        title.setText(Title);
        worth.setText(Worth);
        email.setText(Email);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_startup_profile, container, false);
    }
}