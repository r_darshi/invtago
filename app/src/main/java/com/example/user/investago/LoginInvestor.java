package com.example.user.investago;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LoginInvestor extends AppCompatActivity {

    private EditText email,password;
    private Button signin,register;
    private DatabaseReference mdatabase;
    private FirebaseAuth firebase;
    private String uid;
    ArrayList<LoginUsers> data=new ArrayList<>();
    private SharedPreferences sharedPreferences_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.login_investor);

        email=(EditText)findViewById(R.id.in_email);
        password=(EditText)findViewById(R.id.in_password);
        signin=(Button)findViewById(R.id.sign_in_button);
        register=(Button)findViewById(R.id.in_register);

        //creating a firebase object
        firebase= FirebaseAuth.getInstance();
        mdatabase= FirebaseDatabase.getInstance().getReference().child("investor");



        //action on login button click
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String Email=email.getText().toString().trim();
                final String Password=password.getText().toString().trim();

                if(TextUtils.isEmpty(Email)){
                    email.setError("Email required");
                    return;

                }
                if(TextUtils.isEmpty(Password)){
                    password.setError("Password Required");
                    return;

                }
                final ProgressDialog progressDialog=new ProgressDialog(LoginInvestor.this);
                progressDialog.setTitle("Logging You In...");
                progressDialog.show();
                progressDialog.setCancelable(false);

                firebase.signInWithEmailAndPassword(Email,Password)
                        .addOnCompleteListener(LoginInvestor.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if(task.isSuccessful()){
                                    String uid=firebase.getCurrentUser().getUid();
                                    progressDialog.dismiss();
                                    getDataFromServer(uid);

                                    Intent i = new Intent(LoginInvestor.this, InvesterHome.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  // clearing the stack so that you
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);// dont get back to the login screen
                                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);// on pressing back button
                                    startActivity(i);
                                    finish();
                                }

                                if(!task.isSuccessful()){
                                    progressDialog.dismiss();

                                    if(Password.length()<6)
                                    {
                                        password.setError("Enter a valid password");
                                    }

                                    else{
                                        Toast.makeText(LoginInvestor.this, "email or password incorrect", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                    }
                });

            }
        });


        //start activity register screen
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginInvestor.this,RegisterInvestor.class);
                startActivity(i);
            }
        });


    }
    private void getDataFromServer(final String uid) {
        this.uid=uid;
        mdatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for(DataSnapshot uidDataSnapshot:dataSnapshot.getChildren()){//iterator at location users

                        Log.i("uid",String.valueOf(uidDataSnapshot));//// I/uid: DataSnapshot { key = 1ZmBw1ifdWfEttRZ2aOVlA3L9SO2, value = {email=darshirakhi200@gmail.com, age=46, username=rakhi, first_name=rakhi, last_name=darshi} } -returns for all users in databse
                        //that is datasnapshot prints the key value pair for all node "users'

                        String uidkey= String.valueOf(uidDataSnapshot.getKey());//datasnapshot contains key value pair so uid is the key
                        Log.i("key",uidkey);

                        if(uidkey.equals(uid)){
                            String DetailsOfUser=String.valueOf(uidDataSnapshot.getValue());
                            Log.i("username",DetailsOfUser);//I/username: {email=darshirakhi40@gmail.com, age=84, username=rakhi, first_name=darshirakhi40@gmail.com, last_name=rakhi}
                            StartUpDetails details=uidDataSnapshot.getValue(StartUpDetails.class);
                            Log.i("username",details.getName());//rakhi
                            Log.i("email",details.getEMail());
                            sharedPreferences_in = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor in_editor = sharedPreferences_in.edit();
                            in_editor.putString("investor name", details.getName());
                            in_editor.putString("inv company",details.getCompany());
                            in_editor.putString("inv title",details.getTitle());
                            in_editor.putString("inv worth",details.getWorth());
                            in_editor.putString("inv email",details.getEMail());
                            in_editor.putString("inv uri",details.getUri());


                            in_editor.commit();
                            Log.i("LoginUsername",sharedPreferences_in.getString("name", "default value"));//to check if sharedpreference initialised with value

                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
