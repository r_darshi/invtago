package com.example.user.investago;

public class SuggestionsDetails {

    String investor_name="--",investors_comapny_name="--",comment="--";

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getInvestors_comapny_name() {
        return investors_comapny_name;
    }

    public void setInvestors_comapny_name(String investors_comapny_name) {
        this.investors_comapny_name = investors_comapny_name;
    }

    public String getComment(){
        return comment;
    }
    public void setComment(String comment){
        this.comment=comment;
    }
}