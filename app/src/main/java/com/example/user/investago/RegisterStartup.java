package com.example.user.investago;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

/**
 * Created by Rakhi on 28-09-2017.
 */
public class RegisterStartup extends AppCompatActivity {

    EditText name,company,worth,email,pass,title;
    Button register;
    DatabaseReference ref;
    FirebaseAuth mauth;
    ImageView startup_img;
    FloatingActionButton flt_btn;
    ArrayList<String> filepaths;
    ArrayList<Uri> uris;
    Uri uri;
    Uri uriii;

    StorageReference storageReference;
    private static final int MY_PERMISSIONS_REQUEST = 123;
    private static final int PICK_IMAGE_REQUEST_CODE = 213;
    StartUpDetails details;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.register_startup);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)//gallery permission
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST);
        }


        name=(EditText)findViewById(R.id.reg_name);
        company=(EditText)findViewById(R.id.reg_company);
        worth=(EditText)findViewById(R.id.reg_worth);
        email=(EditText)findViewById(R.id.reg_email);
        pass=(EditText)findViewById(R.id.reg_password);
        register=(Button)findViewById(R.id.register);
        title=(EditText)findViewById(R.id.reg_title);
        ref= FirebaseDatabase.getInstance().getReference().child("startup");
        mauth=FirebaseAuth.getInstance();
        startup_img=(ImageView)findViewById(R.id.startup_img);
        flt_btn=(FloatingActionButton)findViewById(R.id.flt_btn);
        filepaths=new ArrayList<>();
        storageReference= FirebaseStorage.getInstance().getReference();
        uris=new ArrayList<>();



        flt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filepaths.clear();

                FilePickerBuilder.getInstance().setMaxCount(1).setSelectedFiles(filepaths).setActivityTheme(R.style.AppTheme).pickPhoto(RegisterStartup.this);


            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               final String Name=name.getText().toString().trim();
                final String Company=company.getText().toString().trim();
                String Title=title.getText().toString().trim();
                final String Worth=worth.getText().toString().trim();
                final String EMail=email.getText().toString().trim();
                final String Pass=pass.getText().toString().trim();
                String urii=String.valueOf(uriii);


                Log.i("uri",String.valueOf(uri));
                Log.i("uri",String.valueOf(uri));
                Log.i("uri",String.valueOf(uri));
                Log.i("uri",String.valueOf(uri));
                Log.i("uri",String.valueOf(uri));
                Log.i("uri",String.valueOf(uri));

                details = new StartUpDetails();
                details.setName(Name);
                details.setCompany(Company);
                details.setTitle(Title);
                details.setWorth(Worth);
                details.setEMail(EMail);
                details.setPass(Pass);
                details.setUri(urii);


                if(TextUtils.isEmpty(Name) || TextUtils.isEmpty(Company) || TextUtils.isEmpty(Worth)||TextUtils.isEmpty(EMail)||TextUtils.isEmpty(Pass)){
                    Toast.makeText(RegisterStartup.this, "enter all fields value", Toast.LENGTH_SHORT).show();
                }
                else{
                    final ProgressDialog progressDialog = new ProgressDialog(RegisterStartup.this);
                    progressDialog.setTitle("Creating your profile...");
                    progressDialog.show();
                    mauth.createUserWithEmailAndPassword(EMail,Pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            ref.child(mauth.getCurrentUser().getUid()).setValue(details).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Intent i=new Intent(RegisterStartup.this,LoginStartup.class);
                                    startActivity(i);
                                    progressDialog.dismiss();
                                }
                            });

                        }
                        else{
                            Toast.makeText(RegisterStartup.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(RegisterStartup.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if(requestCode == FilePickerConst.REQUEST_CODE_PHOTO && data!=null){
                filepaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);

                for(String path : filepaths){
                    uri = Uri.fromFile(new File(path));
                    Glide.with(RegisterStartup.this).load(uri).into(startup_img);


                    StorageReference reference = storageReference.child("images").child(uri.getLastPathSegment());
                    final ProgressDialog progressDialog = new ProgressDialog(this);
                    progressDialog.setTitle("Creating Your Profile...");
                    progressDialog.show();
                    progressDialog.setCancelable(false);


                    reference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                           uriii=taskSnapshot.getDownloadUrl();/////////////////firebase ka dwnload url dtabase me bhejne k liye////////////////////
                            uris.add(uriii);


                            progressDialog.dismiss();

                            //and displaying a success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                        }
                    })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    //if the upload is not successfull
                                    //hiding the progress dialog
                                    progressDialog.dismiss();

                                    //and displaying error message
                                    Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    //calculating progress percentage
                                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                    //displaying percentage in progress dialog
                                    progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                                }
                            });
                }


            }




        }


    }

}
