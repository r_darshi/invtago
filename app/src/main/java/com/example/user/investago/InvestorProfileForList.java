package com.example.user.investago;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class InvestorProfileForList extends AppCompatActivity {
    ImageView profile;
    TextView name, company, mobile, email, profile_username;
    SharedPreferences sharedPreferences_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_investor_profile);

        profile = (ImageView) findViewById(R.id.profile_image);
        profile_username = (TextView) findViewById(R.id.profile_username);
        name = (TextView) findViewById(R.id.name);
        company = (TextView) findViewById(R.id.comp);
        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);

        Glide.with(InvestorProfileForList.this).load(null).into(profile);
        name.setText("");
        company.setText("");
        email.setText("");
        profile_username.setText("");

        String s = getIntent().getStringExtra("Details");
        String to[] = s.split(";;");

        String Email = to[1];
        String Company = to[0];
        String Name = to[2];
        String Uri = to[3];
        Log.i("CkeckS", s);
        Log.i("CkeckSiowa", Name + " " + Company + " " + Email + " " + Uri);

        Glide.with(InvestorProfileForList.this).load(Uri).into(profile);
        profile_username.setText(Name);
        name.setText(Name);
        company.setText(Company);
        email.setText(Email);
        mobile.setText("07542012513");
    }
}
